btn.addEventListener('click', clickbtn);
let i = 100;
function clickbtn() {
    btn.remove();
    let div = document.createElement('div');
    div.innerHTML = "";
    document.body.append(div);
    let whay = document.body.querySelector('div');
    let input = document.createElement('input');
    input.setAttribute('type', 'number');
    input.setAttribute('placeholder', 'діаметр');
    whay.append(input);
    let button = document.createElement('button');
    button.innerHTML = 'намалювати';
    whay.append(button);
    button.style.marginLeft = '10px';
    button.addEventListener('click', draw);
    function draw() {
        let diametr = input.value;
        let resetDiv = button.closest('div');
        resetDiv.remove();
        let divRound = document.createElement('div');
        divRound.innerHTML = '';
        divRound.addEventListener('click', event => {
            console.log('event', event)
            console.log('event.target', event.target)
            if(event.target.classList.contains('circle')){
                event.target.remove()
            }
        })
        document.body.append(divRound);
        let whayRounds = document.body.querySelector('div');
        console.log('whayRounds', whayRounds)
        for (let n = 0; n < i; n++) {
            let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
            circle = document.createElement('div');
            circle.classList.add('circle')
            circle.innerHTML = '';
            whayRounds.append(circle);
            circle.style.display = 'inline-block';
            circle.style.width = diametr + "px";
            circle.style.height = diametr + 'px';
            circle.style.border = '1px solid black';
            circle.style.borderRadius = '50%'
            circle.style.backgroundColor = randomColor;
            circle.style.marginLeft = '15px';
            circle.style.cursor = 'pointer'
        }
    }
}